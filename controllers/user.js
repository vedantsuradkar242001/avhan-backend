const User = require("../models/User"); 
const bcrypt = require("bcrypt");
const auth = require("../auth");



module.exports.checkTeamNameExists = (reqBody) => {
	return User.find({teamName : reqBody.teamName}).then(result => { 

		if(result.length > 0){
			return true;
		}

		else {
			return false;
		}
	})
}



module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		capFirstName : reqBody.capFirstName,
		capLastName : reqBody.capLastName,
		capMobileNo : reqBody.capMobileNo,
		vcapFirstName : reqBody.vcapFirstName,
		vcapLastName : reqBody.vcapLastName,
		vcapMobileNo : reqBody.vcapMobileNo,
		teamName : reqBody.teamName,
		
		password : bcrypt.hashSync(reqBody.password, 10)
	})


	return newUser.save().then((user, error) => {
	
		if(error){
			return false;
		}

		else{
			return true;
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({teamName : reqBody.teamName}).then(result => {

		if(result == null){
			return false
		}

		{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			}
		
			else{
				return false;
			}
		}
	})
}

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

	result.password = "";

		return result;

	});

};


