
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	capFirstName : {
		type : String,
		required : [true, "First name is required"]
	},
	capLastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	capMobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	vcapFirstName : {
		type : String,
		required : [true, "First name is required"]
	},
	vcapLastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	vcapMobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	members : [
		{
			name : {
				type : String
				
			},
			MobileNo : {
				type : String
				
			}
			
		}
	],
	teamName:{
		type : String,
		required : [true, "name required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	}
	
})

module.exports = mongoose.model("User", userSchema);
